package com.fireball1725.skeletonmod.reference;

public class ModInfo {
    public static final String MOD_ID = "modid";
    public static final String MOD_NAME = "Mod Name";
    public static final String VERSION_BUILD = "@VERSION@";
    public static final String VERSION_BUILD_NUMBER = "@BUILDNUMBER@";
    public static final String AUTOUPDATE_ID = "";
    public static final String DEPENDENCIES = "";
    public static final String SERVER_PROXY_CLASS = "com.fireball1725.skeletonmod.proxy.ServerProxy";
    public static final String CLIENT_PROXY_CLASS = "com.fireball1725.skeletonmod.proxy.ClientProxy";
}
