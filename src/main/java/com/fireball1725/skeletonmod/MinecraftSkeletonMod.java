package com.fireball1725.skeletonmod;

import com.fireball1725.skeletonmod.helpers.LogHelper;
import com.fireball1725.skeletonmod.proxy.IProxy;
import com.fireball1725.skeletonmod.reference.ModInfo;
import com.google.common.base.Stopwatch;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;

import java.util.concurrent.TimeUnit;

@Mod(modid = ModInfo.MOD_ID, name = ModInfo.MOD_NAME, dependencies = ModInfo.DEPENDENCIES, version = ModInfo.VERSION_BUILD)
public class MinecraftSkeletonMod {
    @Mod.Instance
    public static MinecraftSkeletonMod instance;

    @SidedProxy(clientSide = ModInfo.CLIENT_PROXY_CLASS, serverSide = ModInfo.SERVER_PROXY_CLASS)
    public static IProxy proxy;

    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        Stopwatch stopwatch = Stopwatch.createStarted();
        LogHelper.info("Pre Initialization ( started )");

        // Proxy Pre-Init

        LogHelper.info("Pre Initialization ( ended after " + stopwatch.elapsed(TimeUnit.MILLISECONDS) + "ms )");
    }

    @Mod.EventHandler
    public void init(FMLInitializationEvent event) {
        Stopwatch stopwatch = Stopwatch.createStarted();
        LogHelper.info("Initialization ( started )");

        // Proxy Init

        LogHelper.info("Initialization ( ended after " + stopwatch.elapsed(TimeUnit.MILLISECONDS) + "ms )");
    }

    @Mod.EventHandler
    public void postInit(FMLPostInitializationEvent event) {
        Stopwatch stopwatch = Stopwatch.createStarted();
        LogHelper.info("Post Initialization ( started )");

        // Proxy Post-Init

        LogHelper.info("Post Initialization ( ended after " + stopwatch.elapsed(TimeUnit.MILLISECONDS) + "ms )");
    }
}
