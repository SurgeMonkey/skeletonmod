package com.fireball1725.skeletonmod.tileentity;

import com.fireball1725.skeletonmod.util.ItemStackSrc;
import net.minecraft.block.Block;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.Packet;
import net.minecraft.network.play.server.S35PacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

import java.util.ArrayList;
import java.util.HashMap;

public class TileEntityBase extends TileEntity {
    public static final HashMap<Class, ItemStackSrc> myItem = new HashMap();
    public String customName;
    public int renderedFragment = 0;

    public static void registerTileItem(Class c, ItemStackSrc wat) {
        myItem.put(c, wat);
    }

    @Override
    public Packet getDescriptionPacket() {
        NBTTagCompound data = new NBTTagCompound();
        writeToNBT(data);
        return new S35PacketUpdateTileEntity(this.xCoord, this.yCoord, this.zCoord, 1, data);
    }

    @Override
    public void onDataPacket(NetworkManager networkManager, S35PacketUpdateTileEntity s35PacketUpdateTileEntity) {
        readFromNBT(s35PacketUpdateTileEntity.func_148857_g());
        worldObj.markBlockRangeForRenderUpdate(xCoord, yCoord, zCoord, xCoord, yCoord, zCoord);
        markForUpdate();
    }

    public void markForUpdate() {
        if (this.renderedFragment > 0) {
            this.renderedFragment |= 0x1;
        } else if (this.worldObj != null) {
            this.worldObj.markBlockForUpdate(this.xCoord, this.yCoord, this.zCoord);

            Block block = worldObj.getBlock(xCoord, yCoord, zCoord);

            this.worldObj.notifyBlocksOfNeighborChange(xCoord, yCoord - 1, zCoord, block);
            this.worldObj.notifyBlocksOfNeighborChange(xCoord, yCoord + 1, zCoord, block);
            this.worldObj.notifyBlocksOfNeighborChange(xCoord - 1, yCoord, zCoord, block);
            this.worldObj.notifyBlocksOfNeighborChange(xCoord + 1, yCoord, zCoord, block);
            this.worldObj.notifyBlocksOfNeighborChange(xCoord, yCoord - 1, zCoord - 1, block);
            this.worldObj.notifyBlocksOfNeighborChange(xCoord, yCoord - 1, zCoord + 1, block);
        }
    }

    public boolean notLoaded() {
        return !this.worldObj.blockExists(this.xCoord, this.yCoord, this.zCoord);
    }

    public void onChunkLoad() {
        if (this.isInvalid())
            this.validate();

        markForUpdate();
    }

    @Override
    public void onChunkUnload() {
        if (!this.isInvalid())
            this.invalidate();
    }

    public void getDrops(World w, int x, int y, int z, ArrayList<ItemStack> drops) {
        if (this instanceof IInventory) {
            IInventory inventory = (IInventory) this;
            for (int l = 0; l < inventory.getSizeInventory(); l++) {
                ItemStack itemStack = inventory.getStackInSlot(l);
                if (itemStack != null) {
                    drops.add(itemStack);
                }
            }
        }
    }

    protected ItemStack getItemFromTile(Object object) {
        ItemStackSrc itemStackSrc = (ItemStackSrc) myItem.get(object.getClass());
        if (itemStackSrc == null) {
            return null;
        }
        return itemStackSrc.stack(1);
    }

    public TileEntity getTile() {
        return this;
    }

    public String getCustomName() {
        return hasCustomName() ? this.customName : getClass().getSimpleName();
    }

    public boolean hasCustomName() {
        return (this.customName != null) && (this.customName.length() > 0);
    }

    public void setName(String name) {
        this.customName = name;
    }

    @Override
    public void writeToNBT(NBTTagCompound nbtTagCompound) {
        super.writeToNBT(nbtTagCompound);

        if (this.customName != null) {
            nbtTagCompound.setString("customName", this.customName);
        }
    }

    @Override
    public void readFromNBT(NBTTagCompound nbtTagCompound) {
        super.readFromNBT(nbtTagCompound);

        if (nbtTagCompound.hasKey("customName")) {
            this.customName = nbtTagCompound.getString("customName");
        } else {
            this.customName = null;
        }
    }
}
